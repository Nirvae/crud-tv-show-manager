﻿/*
Script de déploiement pour TVShowDB

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "TVShowDB"
:setvar DefaultFilePrefix "TVShowDB"
:setvar DefaultDataPath "C:\Users\megax\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"
:setvar DefaultLogPath "C:\Users\megax\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'L''opération suivante a été générée à partir d''un fichier journal de refactorisation 0db55aac-e2c9-4135-bc4b-e01ca7c28c45';

PRINT N'Renommer [dbo].[User].[Emailaddress] en lol';


GO
EXECUTE sp_rename @objname = N'[dbo].[User].[Emailaddress]', @newname = N'lol', @objtype = N'COLUMN';


GO
-- Étape de refactorisation pour mettre à jour le serveur cible avec des journaux de transactions déployés
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '0db55aac-e2c9-4135-bc4b-e01ca7c28c45')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('0db55aac-e2c9-4135-bc4b-e01ca7c28c45')

GO

GO
PRINT N'Mise à jour terminée.';


GO
