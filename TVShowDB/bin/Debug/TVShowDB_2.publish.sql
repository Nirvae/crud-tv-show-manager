﻿/*
Script de déploiement pour TVShowDB

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "TVShowDB"
:setvar DefaultFilePrefix "TVShowDB"
:setvar DefaultDataPath "C:\Users\megax\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"
:setvar DefaultLogPath "C:\Users\megax\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'L''opération de refactorisation de changement de nom avec la clé dbfe20a1-4bdc-4f93-8a7d-c49522a25fa7 est ignorée, l''élément [dbo].[User].[UserName] (SqlSimpleColumn) ne sera pas renommé en Username';


GO
PRINT N'L''opération suivante a été générée à partir d''un fichier journal de refactorisation 107493e6-25bc-42e4-9d75-e8de9326acad';

PRINT N'Renommer [dbo].[User].[Email] en Emailaddress';


GO
EXECUTE sp_rename @objname = N'[dbo].[User].[Email]', @newname = N'Emailaddress', @objtype = N'COLUMN';


GO
PRINT N'L''opération suivante a été générée à partir d''un fichier journal de refactorisation db506fd6-b10a-4c82-a549-16b15d06a7ae';

PRINT N'Renommer [dbo].[User].[Password] en PasswordHash';


GO
EXECUTE sp_rename @objname = N'[dbo].[User].[Password]', @newname = N'PasswordHash', @objtype = N'COLUMN';


GO
-- Étape de refactorisation pour mettre à jour le serveur cible avec des journaux de transactions déployés

IF OBJECT_ID(N'dbo.__RefactorLog') IS NULL
BEGIN
    CREATE TABLE [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
    EXEC sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
END
GO
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = 'dbfe20a1-4bdc-4f93-8a7d-c49522a25fa7')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('dbfe20a1-4bdc-4f93-8a7d-c49522a25fa7')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '107493e6-25bc-42e4-9d75-e8de9326acad')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('107493e6-25bc-42e4-9d75-e8de9326acad')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = 'db506fd6-b10a-4c82-a549-16b15d06a7ae')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('db506fd6-b10a-4c82-a549-16b15d06a7ae')

GO

GO
PRINT N'Mise à jour terminée.';


GO
