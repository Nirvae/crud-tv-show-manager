﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.BusinessLogic
{
    public static class ShowProcessor
    {
        public static int CreateShow(string name, string producer, string runtime)
        {
            ShowModel data = new ShowModel
            {
                Name = name,
                Producer = producer,
                Runtime = runtime
            };

            string sql = "insert into dbo.Shows (Name, Producer, Runtime) values (@Name, @Producer, @Runtime);";

            return SqlDataAccess.SaveData(sql, data);
        }

        public static List<ShowModel> LoadShows()
        {
            string sql = @"select Id, Name, Producer, Runtime
                           from dbo.Shows;";

            return SqlDataAccess.LoadData<ShowModel>(sql);
        }

        public static List<ShowModel> LoadShow(int id)
        {
            string sql = @"SELECT Id, Name, Producer, Runtime
                           FROM dbo.Shows WHERE id = " + id + ";";

            return SqlDataAccess.LoadData<ShowModel>(sql);
        }



        public static int DeleteShowBdd(int id)
        {
            string sql = "DELETE FROM dbo.Shows WHERE id = " + id + ";";

            return SqlDataAccess.DeleteEditData(sql);
        }


        public static int EditShowBdd(string Name, string Producer, string Runtime, int id)
        {
            string sql = @"UPDATE dbo.Shows 
                            SET Name = '" + Name + "' , Producer = '" + Producer + "' , Runtime = '" + Runtime + @"'
                            WHERE id = " + id + ";";
            System.Diagnostics.Debug.WriteLine(sql);
            return SqlDataAccess.DeleteEditData(sql);
        }
    }
}
