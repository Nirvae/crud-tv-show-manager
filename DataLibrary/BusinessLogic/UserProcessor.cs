﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.BusinessLogic
{
    public static class UserProcessor
    {
        public static int CreateUser(string username, string emailAddress, string passwordHash)
        {
            UserModel data = new UserModel
            {
                Username = username,
                EmailAddress = emailAddress,
                PasswordHash = passwordHash
            };

            string sql = "insert into dbo.Users (Username, EmailAddress, PasswordHash) values (@Username, @EmailAddress, @PasswordHash);";

            return SqlDataAccess.SaveData(sql, data);
        }

        public static List<UserModel> LoadUsers()
        {
            string sql = @"select Id, Username, EmailAddress, PasswordHash
                           from dbo.Users;";

            return SqlDataAccess.LoadData<UserModel>(sql);
        }
    }
}
