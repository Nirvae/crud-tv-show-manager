﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataLibrary.DataAccess
{
    public class SqlDataAccess
    {
        public static string GetConnectionstring(string connectionName = "TVShowDB")
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }

        public static List<T> LoadData<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionstring()))
            {
                return cnn.Query<T>(sql).ToList();
            }
        }

        public static int SaveData<T>(string sql, T data)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionstring()))
            {
                return cnn.Execute(sql, data);
            }
        }

        public static int DeleteEditData(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionstring()))
            {
                return cnn.Execute(sql);
            }
        }
    }
}
