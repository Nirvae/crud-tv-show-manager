﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.Models
{
    public class ShowModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Producer { get; set; }
        public string Runtime { get; set; }
    }
}
