﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVShowManager.Models;
using static DataLibrary.BusinessLogic.UserProcessor;
using static DataLibrary.BusinessLogic.ShowProcessor;

namespace TVShowManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("ViewShows");
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ViewUsers()
        {
            ViewBag.Message = "User list";

            var data = LoadUsers();
            List<UserModel> users = new List<UserModel>();

            foreach (var row in data)
            {
                users.Add(new UserModel
                {
                    UserName = row.Username,
                    Email = row.EmailAddress,
                    Password = row.PasswordHash,
                });
            }

            return View(users);
        }

        public ActionResult ViewShows()
        {
            ViewBag.Message = "TV Shows list";

            var data = LoadShows();
            List<ShowModel> shows = new List<ShowModel>();

            foreach (var row in data)
            {
                shows.Add(new ShowModel
                {
                    Name = row.Name,
                    Producer = row.Producer,
                    Runtime = row.Runtime,
                    Id = row.id,
                });
            }

            return View(shows);
        }

        public ActionResult EditShow(int id)
        {
            var data = LoadShow(id);
            ShowModel show = new ShowModel
            {
                Name = data[0].Name,
                Producer = data[0].Producer,
                Runtime = data[0].Runtime,
                Id = data[0].id
            };

            return View(show);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditShow(ShowModel show)
        {
            System.Diagnostics.Debug.WriteLine(show.Name);

            EditShowBdd(show.Name, show.Producer, show.Runtime, show.Id);

            return RedirectToAction("ViewShows");
        }


            public ActionResult DeleteShow(int id)
        {
            DeleteShowBdd(id);
            return RedirectToAction("ViewShows");
        }

        public ActionResult SignUp()
        {
            ViewBag.Message = "User Sign Up.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(UserModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateUser(model.UserName, model.Email, model.Password.GetHashCode().ToString());
                return RedirectToAction("ViewShows");
            }

            return View();
        }

        public ActionResult AddShow()
        {
            ViewBag.Message = "Add a TV Show.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddShow(ShowModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateShow(model.Name, model.Producer, model.Runtime);
                return RedirectToAction("ViewShows");
            }

            return View();
        }
    }
}