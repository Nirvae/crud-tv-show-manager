﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TVShowManager.Models
{
    public class ShowModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Name of the show")]
        [Required(ErrorMessage = "You need to enter a name.")]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Producer")]
        [Required(ErrorMessage = "You need to enter a producer.")]
        public string Producer { get; set; }

        [Display(Name = "Episode lenght")]
        [Required(ErrorMessage = "A show must have a runtime")]
        [DataType(DataType.Time)]
        public string Runtime { get; set; }

        public int Id { get; set; }
    }
}